# List of components from 3rd-party projects
This file contains a list of all 3rd-party component redistributed
with NitrokeyAuthenticator. All components in this directory are integral
part of NitrokeyAuthenticator and are always shipped with this application,
unlike some other 3rd-party tools, that are not redstributed (e.g. GoogleTest),
but used on demand (user has to download those separately e.g. using
`git submodule`).

## Icons from Nitrokey-app
The icons are used in *About* dialog box, as well as `.desktop` file, to provide
the icon in system's Application Menu.

* [Homepage](https://github.com/Nitrokey/nitrokey-app)
* [Link to icons within Nitrokey-app](https://github.com/Nitrokey/nitrokey-app/tree/master/data/icons/hicolor)
