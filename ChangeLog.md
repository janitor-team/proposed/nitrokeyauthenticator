# ChangeLog

## 1.2.1
* Minor names' fixes in .desktop file

## 1.2
* Added AppStream file.

## 1.1
* Renamed icon 3rd-party icon theme from *nitrokey-app* to *nitrokey-authenticator*. This fixes a [bug in Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=973628)

## 1.0
* Initial release
