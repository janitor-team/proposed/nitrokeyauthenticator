cmake_minimum_required(VERSION 3.5)
set(NITROKEY_AUTHENTICATOR_VERSION "1.2.1")

project(NitrokeyAuthenticator LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5 COMPONENTS DBus Core Widgets LinguistTools Test REQUIRED)
find_package(Boost COMPONENTS log REQUIRED)
add_compile_definitions(BOOST_LOG_DYN_LINK)
find_package(PkgConfig)
pkg_search_module(HIDAPI_LIBUSB REQUIRED hidapi-libusb)
pkg_search_module(ZBARQT REQUIRED zbar-qt)

configure_file(src/applicationglobal.h.in applicationglobal.h)
configure_file(src/NitrokeyAuthenticator.desktop.in NitrokeyAuthenticator.desktop)
option(PACKAGE_TESTS "Build the tests" OFF)
if(PACKAGE_TESTS)
    include(CTest)
    enable_testing()
    include(GoogleTest)
    add_subdirectory(test)
endif()

option(USE_BUNDLED_LIBNITROKEY "Build using bundled libnitrokey, instead of the one installed system-wide" ON)

if(USE_BUNDLED_LIBNITROKEY)
    add_subdirectory (libnitrokey)
    include_directories(libnitrokey)
else()
    pkg_search_module(LIBNITROKEY REQUIRED libnitrokey-1)
endif()

set(TS_FILES src/NitrokeyAuthenticator_pl_PL.ts)

set(NitrokeyAuthenticator_src
    src/aboutnitrokeydialog.cpp
    src/aboutnitrokeydialog.h
    src/aboutnitrokeydialog.ui
    src/authentication.cpp
    src/authentication.h
    src/authenticationexception.cpp
    src/authenticationexception.h
    src/authenticatedialog.cpp
    src/authenticatedialog.h
    src/authenticatedialog.ui
    src/connectionstate.cpp
    src/connectionstate.h
    src/gettotpcoderesponse.h
    src/keytotpmodel.cpp
    src/keytotpmodel.h
    src/nitrokeybase.cpp
    src/nitrokeybase.h
    src/nitrokeyimpl.cpp
    src/nitrokeyimpl.h
    src/nitrokeyprovider.cpp
    src/nitrokeyprovider.h
    src/main.cpp
    src/mainwindow.cpp
    src/mainwindow.h
    src/mainwindow.ui
    src/newtotpslotdialog.cpp
    src/newtotpslotdialog.h
    src/newtotpslotdialog.ui
    src/newtotpslotprovider.cpp
    src/newtotpslotprovider.h
    src/totpuriparser.cpp
    src/totpuriparser.h
    src/totpslot.h
    src/totpconverter.h
    src/totpconverter.cpp
    src/randompasswordgenerator.h
    src/randompasswordgenerator.cpp
    src/screenshotter.cpp
    src/screenshotter.h
    src/unauthenticatedexception.cpp
    src/unauthenticatedexception.h
    src/waylandscreenshotexception.cpp
    src/waylandscreenshotexception.h
    src/newtotpslotprovider.h
    src/newtotpslotprovider.cpp
    src/noemptyslotexception.h
    src/noemptyslotexception.cpp
    src/duplicateslotexception.h
    src/duplicateslotexception.cpp

    resources.qrc
)
if(ANDROID)
  add_library(NitrokeyAuthenticator SHARED
    ${NitrokeyAuthenticator_src}
    ${TS_FILES}
  )
else()
  add_executable(NitrokeyAuthenticator
      ${NitrokeyAuthenticator_src}
    ${TS_FILES}
  )
endif()

include_directories(${CMAKE_BINARY_DIR})
target_include_directories(NitrokeyAuthenticator PUBLIC ${LIB_NITROKEY_INCLUDE_PATH})
target_link_libraries(NitrokeyAuthenticator Qt5::DBus Qt5::Widgets)
if(USE_BUNDLED_LIBNITROKEY)
    target_link_libraries(NitrokeyAuthenticator nitrokey)
else()
    target_compile_options(NitrokeyAuthenticator PRIVATE ${LIBNITROKEY_CFLAGS})
    target_link_libraries(NitrokeyAuthenticator ${LIBNITROKEY_LDFLAGS})
endif()
target_compile_options(NitrokeyAuthenticator PRIVATE ${HIDAPI_LIBUSB_CFLAGS})
target_link_libraries(NitrokeyAuthenticator ${HIDAPI_LIBUSB_LDFLAGS})
target_compile_options(NitrokeyAuthenticator PRIVATE ${ZBARQT_CFLAGS})
target_link_libraries(NitrokeyAuthenticator ${ZBARQT_LDFLAGS})
target_link_libraries(NitrokeyAuthenticator ${Boost_LIBRARIES})

# Installing
install(TARGETS NitrokeyAuthenticator DESTINATION bin)
install(FILES ${CMAKE_BINARY_DIR}/NitrokeyAuthenticator.desktop
    DESTINATION ${CMAKE_INSTALL_PREFIX}/share/applications)
install(FILES ${CMAKE_SOURCE_DIR}/data/nitrokey-authenticator.appdata.xml
    DESTINATION ${CMAKE_INSTALL_PREFIX}/share/metainfo/)
install(DIRECTORY 3rd_party/icons/
    DESTINATION ${CMAKE_INSTALL_PREFIX}/share/icons
)
