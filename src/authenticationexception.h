#ifndef AUTHENTICATIONEXCEPTION_H
#define AUTHENTICATIONEXCEPTION_H

#include <stdexcept>

/**
 * @brief Exception thrown on authentication error.
 */
class AuthenticationException : public std::runtime_error
{
public:
    /**
     * @brief Constructs instance of the exception.
     * @param message Error message.
     */
    AuthenticationException(const std::string &message);
};

#endif // AUTHENTICATIONEXCEPTION_H
