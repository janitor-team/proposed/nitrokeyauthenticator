#ifndef NEWTOTPSLOTDIALOGMOCK_H
#define NEWTOTPSLOTDIALOGMOCK_H

#include <gmock/gmock.h>

#include "newtotpslotdialog.h"

class NewTOTPSlotDialogMock : public NewTOTPSlotDialog
{
public:
    MOCK_METHOD(int, exec, (), (override));
    MOCK_METHOD(QString, slotName, (), (const, override));
};

#endif // NEWTOTPSLOTDIALOGMOCK_H
