#ifndef AUTHENTICATIONDIALOGMOCK_H
#define AUTHENTICATIONDIALOGMOCK_H

#include <gmock/gmock.h>

#include "authenticatedialog.h"

class AuthenticateDialogMock : public AuthenticateDialog
{
public:
    AuthenticateDialogMock(AuthenticateDialog::Type type)
        : AuthenticateDialog(type) {}

    MOCK_METHOD(int, exec, (), (override));
    MOCK_METHOD(QString, pin, (), (const, override));
};

#endif // AUTHENTICATIONDIALOGMOCK_H
