Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nitrokey-authenticator
Upstream-Contact: Agnieszka Cicha-Cisek <agnieszka@cicha-cisek.pl>, Patryk Cisek <patryk@cisek.email>
Source: https://gitlab.com/Prezu/nitrokeyauthenticator

Files: data/nitrokey-authenticator.appdata.xml
Copyright: 2021 Agnieszka Cicha-Cisek <agnieszka@cicha-cisek.pl>
           2021 Patryk Cisek <patryk@cisek.email>
License: CC0-1.0
 The full content of the license can be found here:
 /usr/share/common-licenses/CC0-1.0

Files: src/* test/* test_data/* .git* licenses/* 3RD_PARTY.md
 ChangeLog.md CMakeLists.txt 3rd_party/README.md Doxyfile LICENSE README.md
 resources.qrc
Copyright: 2020 Agnieszka Cicha-Cisek <agnieszka@cicha-cisek.pl>
           2020 Patryk Cisek <patryk@cisek.email>
License: BSD-2-clause
 BSD 2-Clause License
 .
 Copyright (c) 2020, Agnieszka Cicha-Cisek
 Copyright (c) 2020, Patryk Cisek
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2020 Agnieszka Cicha-Cisek <agnieszka@cicha-cisek.pl>
           2020 Patryk Cisek patryk@debian.org>
License: BSD-2-clause
 BSD 2-Clause License
 .
 Copyright (c) 2020, Agnieszka Cicha-Cisek
 Copyright (c) 2020, Patryk Cisek
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: 3rd_party/icons/*
Copyright: 2012-2020 Nitrokey UG
License: GPL-3+
 The full content of the license can be found here:
 /usr/share/common-licenses/GPL-3
